package com.yunbao.common.bean;

import com.alibaba.fastjson.annotation.JSONField;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class FansUserBean extends UserBean {
    private int mAttention;

    @JSONField(name = "isattent")
    public int getAttention() {
        return mAttention;
    }

    @JSONField(name = "isattent")
    public void setAttention(int attention) {
        mAttention = attention;
    }

    public boolean isAttent() {
        return mAttention == 1;
    }
}
