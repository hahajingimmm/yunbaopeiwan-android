package com.yunbao.common.utils;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.SparseIntArray;

import com.yunbao.common.CommonAppContext;
import com.yunbao.common.Constants;
import com.yunbao.common.R;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class CommonIconUtil {

    private static Drawable sSexMaleDrawble;
    private static Drawable sSexFemaleDrawble;
    private static Drawable sSexMaleBgDrawble;
    private static Drawable sSexFemaleBgDrawble;



    public static Drawable getSexDrawable(int sex) {
        if (sex == 1) {
            if (sSexMaleDrawble == null) {
                sSexMaleDrawble = ContextCompat.getDrawable(CommonAppContext.sInstance, R.mipmap.icon_sex_male);
            }
            return sSexMaleDrawble;
        } else {
            if (sSexFemaleDrawble == null) {
                sSexFemaleDrawble = ContextCompat.getDrawable(CommonAppContext.sInstance, R.mipmap.icon_sex_female);
            }
            return sSexFemaleDrawble;
        }
    }


    public static Drawable getSexBgDrawable(int sex) {
        if (sex == 1) {
            if (sSexMaleBgDrawble == null) {
                sSexMaleBgDrawble = ContextCompat.getDrawable(CommonAppContext.sInstance, R.drawable.bg_sex_male);
            }
            return sSexMaleBgDrawble;
        } else {
            if (sSexFemaleBgDrawble == null) {
                sSexFemaleBgDrawble = ContextCompat.getDrawable(CommonAppContext.sInstance, R.drawable.bg_sex_female);
            }
            return sSexFemaleBgDrawble;
        }
    }



    public static int getSexIcon(int key) {
        return key == 1 ? R.mipmap.icon_sex_male_1 : R.mipmap.icon_sex_female_1;
    }
}
