package com.yunbao.common.bean;

import com.google.gson.annotations.SerializedName;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class ConditionLevel  implements ExportNamer{
    @SerializedName("levelid")
    private String id;
    private String name;

    public ConditionLevel(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public ConditionLevel(){

    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String exportName() {
        return name;
    }

    @Override
    public String exportId() {
        return id;
    }
}
