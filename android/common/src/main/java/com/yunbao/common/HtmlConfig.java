package com.yunbao.common;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class HtmlConfig {

    //提现记录
    public static final String CASH_RECORD = CommonAppConfig.HOST + "/appapi/cash/index?votestype=0";



    //支付宝充值回调地址
    public static final String ALI_PAY_COIN_URL = CommonAppConfig.HOST + "/appapi/pay/notify_ali";
    //支付宝 订单支付 回调地址
    public static final String ALI_PAY_ORDER_URL = CommonAppConfig.HOST + "/appapi/orderback/notify_ali";

    public static final String CHARGE_PRIVCAY = CommonAppConfig.HOST + "/appapi/page/detail?id=2";
    //关于我们
    public static final String ABOUT_US = CommonAppConfig.HOST + "/appapi/page/detail?id=3";
    //全民赚钱
    public static final String MAKE_MONEY = CommonAppConfig.HOST + "/appapi/Agent/share?code=";
    //技能认证
    public static final String SKILL_AUTH = CommonAppConfig.HOST + "/appapi/skillauth/apply?skillid=";

    //注销账号：注销说明  http://yuedantest.yunbaozb.com/appapi/page/detail?id=8
    public static final String LOGOUT_ACCOUNT_URL = CommonAppConfig.HOST + "/appapi/page/detail?id=8";

    // 隐私政策  /appapi/page/detail?id=6
    public static final String SETTING_PRIVACY_POLICY = CommonAppConfig.HOST + "/appapi/page/detail?id=6";

    // 服务协议  /appapi/page/detail?id=9
    public static final String SETTING_SERVICE_PROTOCOL = CommonAppConfig.HOST + "/appapi/page/detail?id=9";

    //收支明细：http://yuedantest.yunbaozb.com/appapi/detail/index&uid=101196&token=
    public static final String INCOME_AND_EXPENSES_DETAIL = CommonAppConfig.HOST + "/appapi/detail/index";

}
