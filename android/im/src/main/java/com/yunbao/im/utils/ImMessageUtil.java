package com.yunbao.im.utils;


import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.im.bean.IMLiveBean;
import com.yunbao.im.bean.ImUserBean;
import com.yunbao.im.interfaces.ImClient;
import java.util.List;
import io.reactivex.Observable;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class ImMessageUtil {

    private static ImMessageUtil sInstance;
    private ImClient mImClient;

    private ImMessageUtil() {
        mImClient = new TxImMessageUtil();
    }

    public static ImMessageUtil getInstance() {
        if (sInstance == null) {
            synchronized (ImMessageUtil.class) {
                if (sInstance == null) {
                    sInstance = new ImMessageUtil();
                }
            }
        }
        return sInstance;
    }

    public void init() {
        if (mImClient != null) {
            mImClient.init();
        }
    }

    /**
     * 登录极光IM
     */
    public void loginImClient(String uid) {
        if (mImClient != null) {
            mImClient.loginImClient(uid);
        }
    }


    /**
     * 登出IM
     */
    public void logoutImClient() {
        if (mImClient != null) {
            mImClient.logoutImClient();
        }
    }



    /**
     * 获取会话的最后一条消息的信息
     */
    public ImUserBean getLastMsgInfo(String uid) {
        if (mImClient != null) {
            return mImClient.getLastMsgInfo(uid);
        }
        return null;
    }


    /**
     * 获取订单列表
     */
    public void getOrderMsgList(CommonCallback<List<String>> callback) {
        if (mImClient != null) {
            mImClient.getOrderMsgList(callback);
        }
    }


    public Observable<List<IMLiveBean>> geSpatchList() {
        if (mImClient != null) {
          return  mImClient.geSpatchList();
        }
        return  null;
    }



    /**
     * 获取某个会话的未读消息数
     */
    public int getUnReadMsgCount(String uid) {
        if (mImClient != null) {
            return mImClient.getUnReadMsgCount(uid);
        }
        return 0;
    }

    /**
     * 刷新全部未读消息的总数
     */
    public void refreshAllUnReadMsgCount() {
        if (mImClient != null) {
            mImClient.refreshAllUnReadMsgCount();
        }
    }

    /**
     * 获取全部未读消息的总数
     */
    public String getAllUnReadMsgCount() {
        if (mImClient != null) {
            return mImClient.getAllUnReadMsgCount();
        }
        return "0";
    }

    /**
     * 设置某个会话的消息为已读
     *
     * @param toUid 对方uid
     */
    public void markAllMessagesAsRead(String toUid, boolean needRefresh) {
        if (mImClient != null) {
            mImClient.markAllMessagesAsRead(toUid, needRefresh);
        }
    }

    /**
     * 标记所有会话为已读  即忽略所有未读
     */
    public void markAllConversationAsRead() {
        if (mImClient != null) {
            mImClient.markAllConversationAsRead();
        }
    }

}
