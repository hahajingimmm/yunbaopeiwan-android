package com.yunbao.im.http;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class ImHttpConsts {
    public static final String GET_IM_USER_INFO = "getImUserInfo";
    public static final String GET_SYSTEM_MESSAGE_LIST = "getSystemMessageList";
    public static final String CHECK_IM = "checkIm";
    public static final String CHARGE_SEND_IM = "chargeSendIm";
    public static final String GET_GIFT_LIST = "getGiftList";
    public static final String SEND_GIFT = "sendGift";
    public static final String CHECK_ORDER = "checkOrder";
    public static final String ORDER_DONE = "orderDone";

    public static final String CALL_GET_START = "start";
    public static final String CALL_GET_STOP = "stop";
    public static final String CALL_GET_USER_STATUS = "getUserstatus";
}
