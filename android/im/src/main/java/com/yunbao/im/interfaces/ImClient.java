package com.yunbao.im.interfaces;

import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.im.bean.IMLiveBean;
import com.yunbao.im.bean.ImUserBean;
import java.util.List;

import io.reactivex.Observable;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public interface ImClient {

    /**
     * 初始化
     */
    void init();

    /**
     * 登录IM
     */
    void loginImClient(String uid);

    /**
     * 登出IM
     */
    void logoutImClient();



    /**
     * 获取会话的最后一条消息的信息
     */
    ImUserBean getLastMsgInfo(String uid);

    /**
     * 获取订单列表
     */
    void getOrderMsgList(CommonCallback<List<String>> callback);


    /**
     * 获取某个会话的未读消息数
     */
    int getUnReadMsgCount(String uid);

    /**
     * 刷新全部未读消息的总数
     */
    void refreshAllUnReadMsgCount();

    /**
     * 获取全部未读消息的总数
     */
    String getAllUnReadMsgCount();


    /**
     * 设置某个会话的消息为已读
     *聊天室
     * @param toUid 对方uid
     */
    void markAllMessagesAsRead(String toUid, boolean needRefresh);

    /**
     * 标记所有会话为已读  即忽略所有未读
     */
    void markAllConversationAsRead();

     Observable<List<IMLiveBean>> geSpatchList();
}
