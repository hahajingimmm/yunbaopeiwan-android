package com.yunbao.main.bean;

import com.chad.library.adapter.base.entity.SectionEntity;
import com.yunbao.common.bean.SkillBean;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class AllSkillSectionBean extends SectionEntity<SkillBean> {

    public AllSkillSectionBean(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public AllSkillSectionBean(SkillBean allSkillBean) {
        super(allSkillBean);
    }
}
