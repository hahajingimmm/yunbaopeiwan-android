package com.yunbao.main.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;
import com.yunbao.common.activity.WebViewActivity;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.OnItemClickListener;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.common.views.AbsMainHomeChildViewHolder;
import com.yunbao.main.R;
import com.yunbao.main.activity.GameActivity;
import com.yunbao.main.adapter.MainHomeRecommendAdapter;
import com.yunbao.main.adapter.MainHomeRecommendClassAdapter;
import com.yunbao.main.adapter.MainHomeRecommendTopAdapter;
import com.yunbao.main.bean.BannerBean;
import com.yunbao.main.bean.ClassBean;
import com.yunbao.main.bean.DynamicUserBean;
import com.yunbao.main.bean.commit.DressingCommitBean;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;
import java.util.ArrayList;
import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class MainHomeRecommendViewHolder extends AbsMainHomeChildViewHolder implements OnItemClickListener<DynamicUserBean>, View.OnClickListener {

    private CommonRefreshView mRefreshView;
    private MainHomeRecommendAdapter mAdapter;
    private DressingCommitBean dressingCommitBean;
    private Banner mBanner;
    private List<BannerBean> mBannerList;
    private boolean mBannerNeedUpdate;
    private MainHomeRecommendTopAdapter mHeadUserAdapter;
    private RecyclerView classRlv;
    private MainHomeRecommendClassAdapter mClassAdapter;
    private List<ClassBean> mClassList;
    private HttpCallback mHomeCallback;


    public MainHomeRecommendViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_home_recommend;
    }

    @Override
    public void init() {
        dressingCommitBean = new DressingCommitBean();
        mRefreshView = (CommonRefreshView) findViewById(R.id.refreshView);
        classRlv=findViewById(R.id.rlv_class);
        mRefreshView.setEmptyTips(WordUtil.getString(R.string.no_more_data_o));
        mRefreshView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mAdapter = new MainHomeRecommendAdapter(mContext);
        mAdapter.setOnItemClickListener(MainHomeRecommendViewHolder.this);
        mRefreshView.setRecyclerViewAdapter(mAdapter);

        getHomeData();
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<DynamicUserBean>() {
            @Override
            public RefreshAdapter<DynamicUserBean> getAdapter() {
                return null;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                MainHttpUtil.getRecommend(dressingCommitBean.getSex(), dressingCommitBean.getAge(), dressingCommitBean.getSkill(), p, callback);
            }

            @Override
            public List<DynamicUserBean> processData(String[] info) {
                JSONObject object = JSON.parseObject(info[0]);
                List<BannerBean> bannerList = JSON.parseArray(object.getString("silidelist"), BannerBean.class);
                List<DynamicUserBean> userList = JSON.parseArray(object.getString("userlist"), DynamicUserBean.class);
                mBannerNeedUpdate = false;

                mAdapter.insertList(userList);

                if (bannerList != null && bannerList.size() > 0) {
                    if (mBannerList == null || mBannerList.size() != bannerList.size()) {
                        mBannerNeedUpdate = true;
                    } else {
                        for (int i = 0; i < mBannerList.size(); i++) {
                            BannerBean bean = mBannerList.get(i);
                            if (bean == null || !bean.isEqual(bannerList.get(i))) {
                                mBannerNeedUpdate = true;
                                break;
                            }
                        }
                    }
                }
                mBannerList = bannerList;
                return userList != null ?
                        userList : new ArrayList<DynamicUserBean>();
            }

            @Override
            public void onRefreshSuccess(List<DynamicUserBean> list, int count) {
                if (mActionListener != null) {
                    mActionListener.onRefreshCompleted();
                }
                showBanner();
                if(mHeadUserAdapter!=null&&mHeadUserAdapter.getItemCount()>0){
                    mRefreshView.hideEmpty();
                }
            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<DynamicUserBean> loadItemList, int loadItemCount) {
            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
        mBanner = findViewById(R.id.banner);
        mBanner.setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object path, ImageView imageView) {
                if (!TextUtils.isEmpty(((BannerBean)path).getImageUrl())){
                    ImgLoader.display(mContext, ((BannerBean) path).getImageUrl(), imageView);
                }

            }
        });
        mBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int p) {
                if (mBannerList != null) {
                    if (p >= 0 && p < mBannerList.size()) {
                        BannerBean bean = mBannerList.get(p);
                        if (bean != null) {
                            String link = bean.getLink();
                            if (!TextUtils.isEmpty(link)) {
                                WebViewActivity.forwardNoLanguage(mContext, link, false);
                            }
                        }
                    }
                }
            }
        });
    }


    private void showBanner() {
        if (mBanner == null) {
            return;
        }
        if (mBannerList == null || mBannerList.size() == 0) {
            return;
        }
        if (mBannerNeedUpdate) {
            mBanner.update(mBannerList);
        }
    }

    @Override
    public void onItemClick(DynamicUserBean bean, int position) {
        forwardUserHome(bean.getId());
    }

    @Override
    public void loadData() {
        if (!isFirstLoadData()) {
            return;
        }
        if (mRefreshView != null) {
            mRefreshView.initData();
        }
    }

    @Override
    public void release() {
        MainHttpUtil.cancel(MainHttpConsts.GET_RECOMMEND);
        mActionListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        release();
    }


    public DressingCommitBean getDressingCommitBean() {
        if (dressingCommitBean == null) {
            dressingCommitBean = new DressingCommitBean();
        }
        return dressingCommitBean;
    }

    public void receiverConditionData(DressingCommitBean dressingCommitBean, boolean needRefresh) {
        if (!needRefresh) {
            return;
        }
        this.dressingCommitBean.copy(dressingCommitBean);
        setFirstLoadData(true);
        loadData();
    }

    @Override
    public void onClick(View v) {
    }

    private void getHomeData() {
        if (mHomeCallback == null) {
            mHomeCallback = new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0 && info.length > 0) {
                        JSONObject obj = JSON.parseObject(info[0]);
                        mClassList = new ArrayList<>();
                        mClassList.addAll(JSON.parseArray(obj.getString("skilllist"), ClassBean.class));
                        classRlv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                        mClassAdapter = new MainHomeRecommendClassAdapter(mContext, mClassList);
                        mClassAdapter.setOnItemClickListener(new OnItemClickListener<ClassBean>() {
                        @Override
                            public void onItemClick(ClassBean bean, int position) {
                            GameActivity.forward(mContext,bean.getId(),bean.getName());
                            }
                        });
                        classRlv.setAdapter(mClassAdapter);

                    }
                }
            };
        }
        MainHttpUtil.getHome(mHomeCallback);
    }

}
