package com.yunbao.main.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.yunbao.common.bean.SkillBean;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class SkillHomeBean extends SkillBean {

    private int mSex;

    @JSONField(name = "sex")
    public int getSex() {
        return mSex;
    }
    @JSONField(name = "sex")
    public void setSex(int sex) {
        mSex = sex;
    }

}
