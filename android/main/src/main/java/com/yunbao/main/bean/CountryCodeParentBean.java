package com.yunbao.main.bean;

import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class CountryCodeParentBean {
    private String title;
    private List<CountryCodeBean> lists;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<CountryCodeBean> getLists() {
        return lists;
    }

    public void setLists(List<CountryCodeBean> lists) {
        this.lists = lists;
    }
}
