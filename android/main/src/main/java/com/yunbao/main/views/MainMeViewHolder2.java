package com.yunbao.main.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.WebViewActivity;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.bean.UserItemBean;
import com.yunbao.common.bean.UserItemBean2;
import com.yunbao.common.custom.SpacesItemDecoration;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.views.AbsMainViewHolder;
import com.yunbao.main.R;
import com.yunbao.main.activity.ChooseSkillActivity;
import com.yunbao.main.activity.FansActivity;
import com.yunbao.main.activity.FollowActivity;
import com.yunbao.main.activity.InviteWebViewActivity;
import com.yunbao.main.activity.MyProfitActivity;
import com.yunbao.main.activity.MySkillActivity;
import com.yunbao.main.activity.OrderCenterActivity;
import com.yunbao.main.activity.SettingActivity;
import com.yunbao.main.activity.WalletActivity;
import com.yunbao.main.adapter.MainMeAdapter2;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;

import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class MainMeViewHolder2 extends AbsMainViewHolder implements View.OnClickListener, MainMeAdapter2.ActionListener {

    private ImageView mAvatar;
    private TextView mName;
    private ImageView iv_anchor_level;
    private ImageView iv_level;
    private TextView mID;
    private TextView mFollow;
    private TextView mFans;
    private boolean mPaused;
    private MainMeAdapter2 mAdapter1;
    private TextView mTv0;
    private TextView mTv3;
    private TextView mMyCoin;

    public MainMeViewHolder2(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_me_2;
    }

    @Override
    public void init() {
        RecyclerView recyclerView1 = (RecyclerView) findViewById(R.id.recyclerView1);
        recyclerView1.setLayoutManager(new GridLayoutManager(mContext, 4, GridLayoutManager.VERTICAL, false));
        mAdapter1 = new MainMeAdapter2(mContext);
        mAdapter1.setActionListener(this);
        recyclerView1.setAdapter(mAdapter1);


        mTv0 = findViewById(R.id.tv0);
        mTv3 = findViewById(R.id.tv3);
        mAvatar = findViewById(R.id.avatar);
        mName = findViewById(R.id.name);
        iv_level = findViewById(R.id.iv_level);
        iv_anchor_level = findViewById(R.id.iv_anchor_level);
        mID = findViewById(R.id.id_val);
        mFollow = findViewById(R.id.follow);
        mFans = findViewById(R.id.fans);
        mMyCoin = findViewById(R.id.myCoin);
        findViewById(R.id.btn_edit).setOnClickListener(this);
        findViewById(R.id.btn_follow).setOnClickListener(this);
        findViewById(R.id.btn_fans).setOnClickListener(this);
        findViewById(R.id.btn_wallet).setOnClickListener(this);
        CommonAppConfig appConfig = CommonAppConfig.getInstance();
        UserBean u = appConfig.getUserBean();
        List<UserItemBean2> list = appConfig.getUserItemList();
        if (u != null) {
            showData(u, list);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isShowed() && mPaused) {
            loadData();
        }
        mPaused = false;
    }

    @Override
    public void loadData() {
        MainHttpUtil.getBaseInfo(mCallback);
    }

    private CommonCallback<UserBean> mCallback = new CommonCallback<UserBean>() {
        @Override
        public void callback(UserBean bean) {
            List<UserItemBean2> list = CommonAppConfig.getInstance().getUserItemList();
            if (bean != null) {
                showData(bean, list);
            }
        }
    };

    private void showData(UserBean u, List<UserItemBean2> list) {
        ImgLoader.displayAvatar(mContext, u.getAvatar(), mAvatar);
        mName.setText(u.getUserNiceName());
        if (u.isShowAnchorLevel()) {
            iv_anchor_level.setVisibility(View.VISIBLE);
            LevelBean anchorBean = CommonAppConfig.getInstance().getAnchorLevel(u.getAnchorLevel());
            ImgLoader.display(mContext, anchorBean.getThumb(), iv_anchor_level);
        } else {
            if (iv_anchor_level.getVisibility() == View.VISIBLE) {
                iv_anchor_level.setVisibility(View.GONE);
            }
        }
        LevelBean levelBean = CommonAppConfig.getInstance().getLevel(u.getLevel());
        ImgLoader.display(mContext, levelBean.getThumb(), iv_level);
        mID.setText(StringUtil.contact("ID:", u.getId()));
        mFollow.setText(StringUtil.toWan(u.getFollowNum()));
        mFans.setText(StringUtil.toWan(u.getFansNum()));
        mMyCoin.setText(u.getCoin());
        if (list != null && list.size() > 0) {

            for (int i = 0; i < list.size(); i++) {
                UserItemBean2 bean = list.get(i);
                if (i == 0) {
                    mTv0.setText(bean.getTitle());
                    mAdapter1.refreshList(bean.getList(),1);
                } else if (i == 1) {
                    mTv3.setText(bean.getTitle());
                }

            }
        }
    }

    @Override
    public void onItemClick(UserItemBean bean) {
        String url = bean.getHref();
        if (!TextUtils.isEmpty(url)) {
            if (url.startsWith(Constants.WEBVIEW_INVITE_SUPERIOR_PREFIX_1)) {
                InviteWebViewActivity.forward(mContext, url);
            } else {
                WebViewActivity.forward(mContext, url);
            }
        } else {
            switch (bean.getId()) {
                case Constants.MAIN_ME_ORDER:
                    OrderCenterActivity.forward(mContext);
                    break;
                case Constants.MAIN_ME_WALLET:
                    forwardWallet();
                    break;
                case Constants.MAIN_ME_AUTH:
                    forwardAuth();
                    break;
                case Constants.MAIN_ME_AUTH_SKILL:
                    ChooseSkillActivity.forward(mContext);
                    break;
                case Constants.MAIN_ME_MY_SKILL:
                    MySkillActivity.forward(mContext);
                    break;
                case Constants.MAIN_ME_SETTING:
                    forwardSetting();
                    break;
                case 15:
                    MyProfitActivity.forward(mContext);
                    break;
            }
        }
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_edit) {
            RouteUtil.forwardUserHome(CommonAppConfig.getInstance().getUid());
        } else if (i == R.id.btn_follow) {
            forwardFollow();
        } else if (i == R.id.btn_fans) {
            forwardFans();
        }else if (i == R.id.btn_wallet) {
            forwardWallet();
        }
    }

    /**
     * 我的关注
     */
    private void forwardFollow() {
        FollowActivity.forward(mContext, CommonAppConfig.getInstance().getUid());
    }

    /**
     * 我的粉丝
     */
    private void forwardFans() {
        FansActivity.forward(mContext, CommonAppConfig.getInstance().getUid());
    }


    /**
     * 我要认证
     */
    private void forwardAuth() {
        UserBean u = CommonAppConfig.getInstance().getUserBean();
    }


    /**
     * 我的钱包
     */
    private void forwardWallet() {
        mContext.startActivity(new Intent(mContext, WalletActivity.class));
    }


    /**
     * 设置
     */
    private void forwardSetting() {
        mContext.startActivity(new Intent(mContext, SettingActivity.class));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MainHttpUtil.cancel(MainHttpConsts.GET_BASE_INFO);
    }
}
