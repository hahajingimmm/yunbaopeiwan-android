package com.yunbao.main.bean;

import com.alibaba.fastjson.annotation.JSONField;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class SkillPriceTipBean {

    private String mSkillPrice;
    private String mOrderNum;

    @JSONField(name = "coin")
    public String getSkillPrice() {
        return mSkillPrice;
    }

    @JSONField(name = "coin")
    public void setSkillPrice(String skillPrice) {
        mSkillPrice = skillPrice;
    }

    @JSONField(name = "orders")
    public String getOrderNum() {
        return mOrderNum;
    }

    @JSONField(name = "orders")
    public void setOrderNum(String orderNum) {
        mOrderNum = orderNum;
    }
}
