package com.yunbao.main.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lzy.okgo.model.Response;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.CommonAppContext;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.activity.WebViewActivity;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.event.LoginSuccessEvent;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.http.JsonBean;
import com.yunbao.common.utils.SpUtil;
import com.yunbao.common.utils.ToastHigherUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.http.MainHttpUtil;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.HashMap;
import java.util.Map;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class LoginActivity extends AbsActivity{

    private boolean mHasGetCode;
    private HttpCallback mGetCodeCallback;
    private Handler mHandler;
    private int TOTAL = 60;
    private int mCount = TOTAL;
    private String mGetCodeAgain;
    private TextView mBtnBottomTip;
    private TextView tv_phone_code;
    private boolean mIsAgreeTerms = false;
    private CheckBox cb_read_terms;

    public static void forward() {
        Intent intent = new Intent(CommonAppContext.sInstance, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        CommonAppContext.sInstance.startActivity(intent);
    }

    private EditText mPhoneNum;//手机号
    private View mBtnLogin;
    private String mLoginType = Constants.MOB_PHONE;//登录方式

    private TextView mEditCode;
    private TextView mBtnGetCode;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isStatusBarWhite() {
        return true;
    }

    @Override
    protected void main() {
        mPhoneNum = findViewById(R.id.phone_num);
        mEditCode = findViewById(R.id.edit_code);
        tv_phone_code = findViewById(R.id.tv_phone_code);
        mBtnGetCode = findViewById(R.id.btn_get_code);
        mBtnLogin = findViewById(R.id.btn_login);
        mGetCodeAgain = WordUtil.getString(R.string.login_get_code_again);
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                mCount--;
                if (mCount > 0) {
                    mBtnGetCode.setText(mCount + "s");
                    if (mHandler != null) {
                        mHandler.sendEmptyMessageDelayed(0, 1000);
                    }
                } else {
                    mBtnGetCode.setText(mGetCodeAgain);
                    mCount = TOTAL;
                    if (mBtnGetCode != null) {
                        mBtnGetCode.setEnabled(true);
                    }
                }
            }
        };

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String phone = mPhoneNum.getText().toString();
                String code = mEditCode.getText().toString();
                if (phone.length() > 0) {
                    mBtnGetCode.setEnabled(true);
                } else {
                    mBtnGetCode.setEnabled(false);
                }
                mBtnLogin.setEnabled(code.length() == 6 && phone.length() > 0 && mHasGetCode);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        mPhoneNum.addTextChangedListener(textWatcher);
        mEditCode.addTextChangedListener(textWatcher);
        cb_read_terms = findViewById(R.id.cb_read_terms);
        cb_read_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mIsAgreeTerms = isChecked;
            }
        });
        ImageView appIcon = findViewById(R.id.app_icon);
        ImgLoader.display(mContext, CommonAppConfig.getInstance().getAppIconRes(), appIcon);
        TextView appName = findViewById(R.id.app_name);
        appName.setText(WordUtil.getString(R.string.login_tip_3));
        mBtnBottomTip = findViewById(R.id.btn_tip);
        showBottomTips();
        EventBus.getDefault().register(this);
    }

    private void showBottomTips() {
        MainHttpUtil.getLoginInfo(new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0 && info.length > 0) {
                    JSONObject loginInfo = JSON.parseObject(info[0]).getJSONObject("login_alert");
                    if (mBtnBottomTip != null) {
                        String content = loginInfo.getString("login_title");
                        SpannableString spannableString = new SpannableString(content);
                        JSONArray msgArray = JSON.parseArray(loginInfo.getString("message"));
                        for (int i = 0, size = msgArray.size(); i < size; i++) {
                            final JSONObject msgItem = msgArray.getJSONObject(i);
                            String title = msgItem.getString("title");
                            int startIndex = content.indexOf(title);
                            if (startIndex > 0) {
                                ClickableSpan clickableSpan = new ClickableSpan() {
                                    @Override
                                    public void updateDrawState(TextPaint ds) {
                                        super.updateDrawState(ds);
                                        ds.setColor(0xff3399ee);
                                        ds.setUnderlineText(false);
                                    }

                                    @Override
                                    public void onClick(View widget) {
                                        WebViewActivity.forward(mContext, msgItem.getString("url"), false);
                                    }

                                };
                                int endIndex = startIndex + title.length();
                                spannableString.setSpan(clickableSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                            }
                        }

                        mBtnBottomTip.setText(spannableString);
                        mBtnBottomTip.setMovementMethod(LinkMovementMethod.getInstance());//不设置 没有点击事件
                        mBtnBottomTip.setHighlightColor(Color.TRANSPARENT); //设置点击后的颜色为透明
                    }
                }
            }
        });
    }


    public void loginClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_login) {
            login();
        } else if (i == R.id.btn_get_code) {
            getLoginCode();
        } else if (i == R.id.ll_phone_code){
            changePhoneCountryCode();
        }
    }

    private void changePhoneCountryCode(){
        startActivityForResult(new Intent(mContext,ChoosePhoneCountryCodeActivity.class),ChoosePhoneCountryCodeActivity.INTENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChoosePhoneCountryCodeActivity.INTENT_REQUEST_CODE && resultCode == RESULT_OK){
            if (data != null){
                if (tv_phone_code != null){
                    tv_phone_code.setText(data.getStringExtra(Constants.INTENT_PHONE_COUNTRY_CODE));
                }
            }
        }
    }

    private void getLoginCode() {
        String phoneNum = mPhoneNum.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNum)) {
            ToastUtil.show(WordUtil.getString(R.string.login_input_phone));
            mPhoneNum.requestFocus();
            return;
        }
        mEditCode.requestFocus();
        if (mGetCodeCallback == null) {
            mGetCodeCallback = new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0) {
                        mBtnGetCode.setEnabled(false);
                        if (mHandler != null) {
                            mHandler.sendEmptyMessage(0);
                        }
                        if (!TextUtils.isEmpty(msg) && msg.contains("123456")) {
                            ToastUtil.show(msg);
                        }
                    }
                    ToastUtil.show(msg);
                }
            };
        }
        mHasGetCode = true;
        MainHttpUtil.getLoginCode(phoneNum, tv_phone_code.getText().toString(),mGetCodeCallback);
    }


    /**
     * 手机号密码登录
     */
    private void login() {
        if (!mIsAgreeTerms){
            ToastHigherUtil.show(WordUtil.getString(R.string.login_tip_5));
            return;
        }
        final String phoneNum = mPhoneNum.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNum)) {
            ToastUtil.show(WordUtil.getString(R.string.login_input_phone));
            mPhoneNum.requestFocus();
            return;
        }
        if (!mHasGetCode) {
            ToastUtil.show(R.string.login_get_code_please);
            return;
        }

        final String code = mEditCode.getText().toString().trim();
        if (TextUtils.isEmpty(code)) {
            ToastUtil.show(WordUtil.getString(R.string.login_input_code));
            mEditCode.requestFocus();
            return;
        }

        mLoginType = Constants.MOB_PHONE;
        MainHttpUtil.login(phoneNum, code,tv_phone_code.getText().toString(), new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                onLoginSuccess(code, msg, info);
            }

            @Override
            public void onError(Response<JsonBean> response) {
                super.onError(response);
            }
        });
    }


    /**
     * 登录成功！
     */
    private void onLoginSuccess(int code, String msg, String[] info) {
        if (code == 0) {
            if (info.length > 0) {
                try {
                    JSONObject obj = JSON.parseObject(info[0]);
                    String uid = obj.getString("id");
                    String token = obj.getString("token");
                    SpUtil.getInstance().setBooleanValue(Constants.FIRST_LOGIN, obj.getIntValue("isreg") == 1);
                    if (obj.getIntValue("sex") == 0) {
                        CommonAppConfig.getInstance().setLoginInfo(uid, token, false);
                        SetProfileActivity.forward(mContext, info[0], !mLoginType.equals(Constants.MOB_PHONE));
                    } else {
                        CommonAppConfig appConfig = CommonAppConfig.getInstance();
                        appConfig.setLoginInfo(uid, token, true);
                        UserBean userBean = JSON.toJavaObject(obj, UserBean.class);
                        appConfig.setUserBean(userBean);
                        Map<String, String> map = new HashMap<>();
                        map.put(SpUtil.USER_INFO, JSON.toJSONString(userBean));
                        map.put(SpUtil.TX_IM_USER_SIGN, obj.getString("usersig"));
                        SpUtil.getInstance().setMultiStringValue(map);
                        MainActivity.forward(mContext);
                        finish();
                    }
                } catch (Exception e) {

                }
            }
        } else {
            ToastUtil.show(msg);
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginSuccessEvent(LoginSuccessEvent e) {
        finish();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
