package com.example.video.ui.view;

import android.widget.MediaController;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public interface IMediaController extends MediaController.MediaPlayerControl {
    public  void resume();
    public  void play(String url,String conver);
    public  void stop();
    public void setLoop(boolean loop);
    public void setFullScreen(boolean fullScreen);
}
